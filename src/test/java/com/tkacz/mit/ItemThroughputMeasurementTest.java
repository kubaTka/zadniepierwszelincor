package com.tkacz.mit;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.tkacz.mit.rest.ItemWsm;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.UUID;


public class ItemThroughputMeasurementTest {

    /**
     * assumed ratio 10 GETs for 1 POST
     */
    @Test
    public void shouldAnswerWithTrue() throws IOException {
        final ObjectMapper objectMapper = new ObjectMapper();
        final ObjectWriter objectWriter = objectMapper.writerFor(ItemWsm.class);
        try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
            final ArrayList<ItemWsm> itemWsms = new ArrayList<>();
            final ArrayList<HttpPost> httpPosts = new ArrayList<>();
            final ArrayList<HttpGet> httpGets = new ArrayList<>();
            final int max = 100;
            for (int i = 0; i < max; i++) {
                final UUID id = UUID.fromString("221a9d10-f07d-4ef9-bd6b-" + String.format("%012d", i));
                final ItemWsm itemWsm = new ItemWsm(id, "name" + i);
                itemWsms.add(itemWsm);
                final HttpPost httpPost = new HttpPost("http://localhost:8080/items");
                httpPost.setHeader("Content-Type", "application/json");
                httpPost.setEntity(new StringEntity(objectWriter.writeValueAsString(itemWsm)));
                httpPosts.add(httpPost);
                httpGets.add(new HttpGet("http://localhost:8080/items/" + id));
            }
            final long start = System.currentTimeMillis();
            for (int i = 0; i < max; i++) {
                try (CloseableHttpResponse response = httpClient.execute(httpPosts.get(i))) {
                    assert response.getStatusLine().getStatusCode() == 204;
                }
            }
            final long endPost = System.currentTimeMillis();
            for (int i = 0; i < max; i++) {
                for (int j = 0; j < 10; j++) {
                    try (CloseableHttpResponse response = httpClient.execute(httpGets.get(i))) {
                        assert response.getStatusLine().getStatusCode() == 200;
                    }
                }
            }
            final long end = System.currentTimeMillis();
            final long t1 = endPost - start;
            final long t2 = end - endPost;
            final long writesPerSecond = max * 1000 / t1;
            final long readsPerSecond = max * 10000 / t2;
            System.out.println("writes:"+max+" took:"+t1+"ms reads:"+(10*max)+" took:"+t2+"ms ");
            System.out.println("reads per second:"+readsPerSecond);
            System.out.println("writes per second:"+writesPerSecond);
        }


    }
}
