package com.tkacz.mit.repository;

import java.util.Optional;

public class ItemQueryResult {
    private final Optional<String> name;

    public ItemQueryResult(String name) {
        this.name = Optional.of(name);
    }

    public ItemQueryResult() {
        this.name = Optional.empty();
    }

    public Optional<String> getName() {
        return name;
    }
}
