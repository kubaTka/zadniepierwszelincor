package com.tkacz.mit.repository;

import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Component
public class ItemRepository {

    private final Map<UUID, String> items = new HashMap<>();

    public ItemQueryResult find(ItemQuery itemQuery) {
        final String name = items.get(itemQuery.getId());
        if (name == null) {
            return new ItemQueryResult();
        } else {
            return new ItemQueryResult(name);
        }
    }

    public void add(AddItemCommand addItemCommand) {
        items.put(addItemCommand.getId(), addItemCommand.getName());
    }
}
