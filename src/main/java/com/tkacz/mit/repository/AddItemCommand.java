package com.tkacz.mit.repository;

import java.util.UUID;

public class AddItemCommand {
    private final UUID id;
    private final String name;

    public AddItemCommand(UUID id, String name) {
        this.id = id;
        this.name = name;
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
