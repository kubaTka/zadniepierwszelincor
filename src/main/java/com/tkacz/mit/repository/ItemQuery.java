package com.tkacz.mit.repository;

import java.util.UUID;

public class ItemQuery {
    private final UUID id;

    public ItemQuery(UUID id) {
        this.id = id;
    }

    public UUID getId() {
        return id;
    }
}
