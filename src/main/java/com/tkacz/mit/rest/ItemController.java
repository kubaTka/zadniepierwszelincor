package com.tkacz.mit.rest;

import com.tkacz.mit.repository.AddItemCommand;
import com.tkacz.mit.repository.ItemQuery;
import com.tkacz.mit.repository.ItemQueryResult;
import com.tkacz.mit.repository.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/items")
public class ItemController {
    @Autowired
    ItemRepository itemRepository;

    @GetMapping("/{id}")
    public ResponseEntity<ItemWsm> get(@PathVariable UUID id) {
        final ItemQueryResult itemQueryResult = itemRepository.find(new ItemQuery(id));
        final Optional<String> name = itemQueryResult.getName();
        if (name.isPresent()) {
            return new ResponseEntity<>(new ItemWsm(id, name.get()), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    public ResponseEntity<Void> add(@RequestBody ItemWsm itemWsm) {
        itemRepository.add(new AddItemCommand(itemWsm.getId(), itemWsm.getName()));
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}

