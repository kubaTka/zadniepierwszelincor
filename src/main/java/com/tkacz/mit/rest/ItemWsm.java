package com.tkacz.mit.rest;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.UUID;

/**
 * Web service model for Item
 */
public class ItemWsm {

    private final UUID id;
    private final String name;

    public ItemWsm(@JsonProperty("id") UUID id, @JsonProperty("name")String name) {
        this.id = id;
        this.name = name;
    }


    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
