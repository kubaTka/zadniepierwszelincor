package com.tkacz.mit;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ItemApiApp {
    public static void main(String[] args) {
        SpringApplication.run(ItemApiApp.class, args);
    }
}
